FROM alpine
ENV USER="root"
ENV PATH="$PATH:/nix/var/nix/profiles/per-user/root/profile/bin:/nix/var/nix/profiles/default/bin"
RUN apk add curl xz tar
RUN addgroup -g 30000 -S nixbld\
  && for i in $(seq 1 30); do adduser -S -D -h /var/empty -g "Nix build user $i" -u $((30000 + i)) -G nixbld nixbld$i ; done\
  && mkdir -m 0755 /etc/nix\
  && mkdir -m 0755 /nix && chown root /nix\
  && echo 'sandbox = false' > /etc/nix/nix.conf\
  && curl -L "https://github.com/numtide/nix-unstable-installer/releases/download/nix-2.7.0pre20220303_b09baf6/install" | sh\
  && mkdir -p /root/.config/nix && echo "experimental-features = nix-command flakes ca-references" > /root/.config/nix/nix.conf
RUN nix-env -iA nixpkgs.cachix nixpkgs.git nixpkgs.git-lfs
RUN echo 'nix path-info --all | grep -v ".*.drv" | sort > /tmp/store-path-pre-build && cachix use $CACHIX_NAME && cachix authtoken $CACHIX_AUTH_TOKEN && git lfs install' > /bin/pre-build.sh\
  && echo 'nix path-info --all | grep -v ".*.drv" | sort > /tmp/store-path-post-build && echo "$(comm -13 /tmp/store-path-pre-build /tmp/store-path-post-build)" | cachix push $CACHIX_NAME' > /bin/post-build.sh
